coldturkey
======

# About
This is a simple program to simulate the amount of a medication in your body,
given your dose schedule and the medication's half life.

# Usage
## Arguments
 - `-h duration` - half life of medication (**required**), ex `35hours`, ex `'3hours 5min'`
 - `-t size,duration[,perday]` - dose schedule (**one or more `-t` options required**), see examples section, will be treated as continous in the order specified, *perday* defaults to one dose per day
 - `-v` - be verbose, print out a line with information for each hour in the specified -t options
 - `-c csvfile` - save data in specified *csvfile*
 - `-s 'date-in-RFC1123'` - make first dose start at *date-in-RFC1123*, use absolute times in output format like `Mon, 02 Jan 2006 15:04:05 MST` (the day of week has to be there but is ignored), use absolute unix time in csv instead of seconds since first dose

# Examples
 - `go run . -h 35hours -c blar.csv -s 'Mon, 29 Aug 2019 10:00:00 PST' -t 10,2weeks -t 20,7days -t 0,4days -v`
   - specifies a halflife of 35 hours, log output to blar.csv, start at 10AM on 8/29/19
   - specifies a 10mg dose once a day for 2 weeks, then a 20mg dose once a day for 7 days, then a 0mg dose for 4 days(stopping cold turkey)
