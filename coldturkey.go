package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/hako/durafmt"
	"github.com/karrick/tparse/v2"
)

func main() {
	timeSpecs := timeSpec{}
	var halflifestr string
	var verbose bool
	var timestr string
	var filepath string

	flag.Var(&timeSpecs, "t", "timeSpec of format \"size_in_mg,duration[,perday]\", ex: -t \"10,2weeks1day,1\"")
	flag.StringVar(&halflifestr, "h", "", "halflife, time for half of the substance remaining to leave your body")
	flag.BoolVar(&verbose, "v", false, "verbose")
	flag.StringVar(&timestr, "s", "", "time of first dose in RFC1123 format (Mon, 02 Jan 2006 15:04:05 MST)")
	flag.StringVar(&filepath, "c", "", "csv file path for output")

	flag.Parse()

	if len(timeSpecs) == 0 || halflifestr == "" {
		log.Fatal("must provide a halflife and at least one timeSpec.")
	}

	halflife_t, err := tparse.AddDuration(time.Time{}, halflifestr)
	if err != nil {
		log.Fatal("halflife: ", err)
	}

	halflife := halflife_t.Sub(time.Time{})
	starttime := new(time.Time)
	if timestr != "" {
		*starttime, err = time.Parse(time.RFC1123, timestr)
		if err != nil {
			log.Fatal("start time: ", err)
		}
	} else {
		starttime = nil
	}

	var f io.Writer
	if filepath != "" {
		f, err = os.Create(filepath)
		if err != nil {
			log.Fatal("filepath: ", err)
		}
		defer f.(*os.File).Close()
	}

	// log.Println(halflife, timeSpecs)

	var total float64
	var htotal int
	// for each dose definition
	for i, v := range timeSpecs {
		// for each hour in this dose definition
		for h := 0; h < v.Days*24; h++ {
			// if you took a dose during this hour, don't apply it's model until letting the existing value "decay" for this hour first
			var dose float64
			if h%(24/v.PerDay) == 0 {
				dose = float64(v.Size_mg)
			}

			// If something is halved every N elements and travels a smooth exponential decay curve
			// each element is the previous element divided by the Nth root of 2
			// ex. An octave on a piano has 12 keys.  The A above middle C is 440Hz.  The A an
			// octave below that is 220Hz.  The frequency of the G above middle C can be found by
			// 440 / (12th root of 2), and the frequency of a key M keys below A440 can be found by
			// 440 / (12th root of 2)^M or 440 / 2^(M/12)
			// If M is negative, you find the frequency for keys with higher pitch.  For the B
			// above middle C, you can use 440 / 2^(-1/12) or 440 * 2^(1/12)
			// https://en.wikipedia.org/wiki/A440_(pitch_standard)
			// We want decay, so it's always division here
			total /= math.Pow(2, 1/float64(halflife/time.Hour))
			total += dose // actually add the dose after letting existing value "decay"
			if verbose {
				fmt.Println("Total in system at: (", i+1, ")", timeStr(htotal, starttime), ":", total)
			}

			// when logging to file
			if f != nil {
				var t time.Time
				if starttime == nil { // relative time
					t = (time.Unix(0, 0)).Add(time.Hour * time.Duration(htotal))
				} else { // absolute time
					t = starttime.Add(time.Hour * time.Duration(htotal))
				}
				_, err := fmt.Fprintf(f, "%d,%.15f\n", t.Unix(), total)
				if err != nil {
					log.Fatal("csv write: ", err)
				}
			}
			htotal++
		}
	}

	// htotal is incremented one last time to exit the for loop, decrement it so it agrees with the total
	htotal--
	fmt.Println("Total in system at end:", timeStr(htotal, starttime), ":", total)
}

func timeStr(htotal int, t *time.Time) string {
	if t == nil { // relative time
		return durafmt.Parse(time.Hour * time.Duration(htotal)).String()
	} else { // absolute time
		return t.Add(time.Hour * time.Duration(htotal)).Format(time.RFC1123)
	}
}

type timeSpecElem struct {
	Size_mg int
	PerDay  int
	Days    int
}

// everything below here allows flag.Parse to handle my dose definition type (timeSpec)

type timeSpec []timeSpecElem

// Let the flag package print a value of this type
func (t *timeSpec) String() string {
	var s string
	if t == nil {
		return s
	}
	for i, v := range *t {
		then := ", then "
		if i == 0 {
			then = ""
		}
		s += fmt.Sprintf("%s%dmg %d times per day for %d days", then, v.Size_mg, v.PerDay, v.Days)
	}

	return s
}

// This gets called for each -t option on the command line
func (t *timeSpec) Set(s string) error {
	strs := strings.Split(s, ",")

	var sizestr string
	var durationstr string
	perdaystr := "1"

	// get string values of each field, handling optional perday
	if len(strs) == 2 {
		sizestr = strs[0]
		durationstr = strs[1]
	} else if len(strs) == 3 {
		sizestr = strs[0]
		durationstr = strs[1]
		perdaystr = strs[2]
	} else {
		return fmt.Errorf("invalid timeSpec format, expected \"size,duration[,perday]\"")
	}

	// convert strings for each field into actual datatypes
	size, err := strconv.Atoi(sizestr)
	if err != nil {
		return fmt.Errorf("invalid timeSpec format, size: %w", err)
	}
	duration_t, err := tparse.AddDuration(time.Time{}, durationstr)
	if err != nil {
		return fmt.Errorf("invalid timeSpec format, duration: %w", err)
	}
	duration := duration_t.Sub(time.Time{})
	perday, err := strconv.Atoi(perdaystr)
	if err != nil {
		return fmt.Errorf("invalid timeSpec format, perday: %w", err)
	}

	*t = append(*t, timeSpecElem{
		Size_mg: size,
		Days:    int((duration.Truncate(24 * time.Hour)) / (24 * time.Hour)),
		PerDay:  perday,
	})

	return nil
}
