module gitlab.com/rigel314/coldturkey

go 1.13

require (
	github.com/hako/durafmt v0.0.0-20190612201238-650ed9f29a84
	github.com/karrick/tparse/v2 v2.7.0
)
